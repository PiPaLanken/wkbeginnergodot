using Godot;
using System;

public class Enemy : RigidBody2D
{
    public int MaxCiEf = 10;
    public int circlecount = 0;

    const int STARTHEALTH = 1000;
    public int Health;
    public int MaxHealth = STARTHEALTH;
    Player player;
    PackedScene circle;
    Area2D newCircle;
    Timer timer;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        player = (Player)GetNode("/root/Game/Player");
        Health = STARTHEALTH;
        MaxHealth = STARTHEALTH;
        circle = (PackedScene)ResourceLoader.Load("res://CircleEffect.tscn");
        timer = (Timer)GetNode("AttackTimer");

    }
    public override void _Process(float delta)
    {
        if (timer.IsStopped())
        {
            SpawnCircles();
            timer.Start();
        }

    }

    public void Particel()
    {
        var SprialParticle = (CPUParticles2D)GetNode("CircleParticle");
        SprialParticle.Emitting = true;
    }

    public void SpawnCircles()
    {
        for (int i = 0; i < MaxCiEf; i++)
        {
            CloneCircle();
        }
    }
    public void CloneCircle()
    {
        newCircle = (Area2D)circle.Instance();
        AddChild(newCircle);
    }

    public void isDead()
    {
        if (Health <= 0) { GetTree().ChangeScene("res://GameWon.tscn"); }
    }
    public void getDamage(int dmg)
    {
        Health -= player.Damage;
    }
}
