using Godot;
using System;

public class Exit : TextureButton
{
    public override void _Pressed()
    {
        GetTree().ChangeScene("res://Startscreen.tscn");
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
