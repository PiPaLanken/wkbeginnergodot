using Godot;
using System;
public class Player : KinematicBody2D
{
    [Export]
    public int Speed = 250;

    [Export]
    public int JumpSpeed = -900;
    [Export]
    public float gravity = 2500;

    [Signal]
    public delegate void Hit();
    private Vector2 velocity = new Vector2();
    const int STARTHEALTH = 500;
    const int STARTDAMAGE = 100;
    public int Health;
    public int Damage;
    public int MaxHealth = STARTHEALTH;
    Area2D newAttack;
    PackedScene attackScene;
    AnimatedSprite animatedEffect;

    public override void _Ready()
    {
        MaxHealth = STARTHEALTH;
        Health = STARTHEALTH;
        Damage = STARTDAMAGE;
        attackScene = (PackedScene)ResourceLoader.Load("res://Attack.tscn");
        newAttack = (Area2D)attackScene.Instance();

    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public void GetInput()
    {
        velocity.x = 0;
        var right = Input.IsActionPressed("ui_right");
        var left = Input.IsActionPressed("ui_left");
        var jump = Input.IsActionPressed("ui_up");
        var attack = Input.IsActionPressed("ui_control");

        var BloodParticle = (CPUParticles2D)GetNode("/root/Game/Player/PlayerSprite/BloodEffect");
        animatedEffect = (AnimatedSprite)GetNode("/root/Game/Player/PlayerSprite/AttackEffect");
        var animatedSprite = GetNode<AnimatedSprite>("PlayerSprite");
        animatedEffect.Hide();
        var FrameSize = animatedSprite.Frames.GetFrame("Attack", 0).GetSize();
        if (!attack)
        {
            if (animatedEffect.GetChildCount() > 0)
            {
                animatedEffect.RemoveChild(newAttack);
            }
            animatedEffect.Frame = 0;
            if (IsOnFloor() && jump)
            {
                velocity.y = JumpSpeed;
            }
            if (!IsOnFloor()) animatedSprite.Animation = "Jump";
            if (right)
            {
                velocity.x += Speed;
                if (IsOnFloor())
                    animatedSprite.Animation = "Walk";
                animatedSprite.FlipH = true;
                BloodParticle.Position = new Vector2(FrameSize.x - 45f, 15.22f);
                animatedEffect.FlipH = true;
                animatedEffect.Position = new Vector2(FrameSize.x - 35f, 15.22f);

            }
            if (left)
            {
                velocity.x -= Speed;
                if (IsOnFloor())
                    animatedSprite.Animation = "Walk";
                animatedSprite.FlipH = false;
                animatedEffect.FlipH = false;
                BloodParticle.Position = new Vector2(-25f, 15.22f);
                animatedEffect.Position = new Vector2(FrameSize.x - 107f, 15.22f);
            }
            if (!left && !right && IsOnFloor()) animatedSprite.Animation = "Stand";
        }
        if (attack)
        {
            Attacked();
            animatedEffect.Show();
            animatedEffect.Play();
            animatedSprite.Animation = "Attack";
            BloodParticle.Emitting = true;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        velocity.y += gravity * delta;
        GetInput();
        velocity = MoveAndSlide(velocity, new Vector2(0, -1));
    }
    public void Damaged(int dmg)
    {
        GD.Print("Angekommen");
        Health -= dmg;
        GD.Print(Health + " HP Left");
    }
    public void isDead()
    {
        if (Health <= 0)
        {
            GetTree().ChangeScene("res://GameOver.tscn");
        }
    }
    public void Attacked()
    {
        if (animatedEffect.GetChildCount() == 0)
            animatedEffect.AddChild(newAttack);
    }


}

