using Godot;
using System;

public class Attack : Area2D
{
    Enemy enemy;
    Player player;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        enemy = (Enemy)GetNode("/root/Game/Enemy");
        player = (Player)GetNode("/root/Game/Player");
    }

    public void CollisionWithEnemy(RigidBody2D Enemy)
    {
        if (Enemy.Name == "Enemy")
        {
            Enemy.Call("getDamage", player.Damage);
        }
    }
}
