using Godot;
using System;

public class EnemyHealthDisplay : Node2D
{
    Enemy enemy;
    TextureProgress bar;
    Resource bar_red, bar_yellow, bar_green;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        bar_red = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Red.png");
        bar_yellow = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Yellow.png");
        bar_green = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Green.png");
        bar = (TextureProgress)GetNode("EnemyHealthBar");
        enemy = (Enemy)GetNode("/root/Game/Enemy");
        GD.Print("MaxHealth: " + enemy.MaxHealth);
        bar.MaxValue = enemy.MaxHealth;
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        bar.Value = enemy.Health;
        if (enemy.Health <= enemy.MaxHealth * 0.5 && enemy.Health > enemy.MaxHealth * 0.25) { bar.TextureProgress_ = (Texture)bar_yellow; }
        if (enemy.Health <= enemy.MaxHealth * 0.25) { bar.TextureProgress_ = (Texture)bar_red; }
        if (enemy.Health <= 0) { enemy.isDead(); }

    }
}

