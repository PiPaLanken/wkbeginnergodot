using Godot;
using System;

public class HealthDisplay : Node2D
{
    Player player;
    TextureProgress bar;
    Resource bar_red, bar_yellow, bar_green;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        bar_red = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Red.png");
        bar_yellow = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Yellow.png");
        bar_green = ResourceLoader.Load("res://Sprite/GUI/Healthbar_Green.png");
        bar = (TextureProgress)GetNode("HealthBar");
        //var player = GetNode<Player>("/root/Game/Player");
        player = (Player)GetNode("/root/Game/Player");
        GD.Print("MaxHealth: " + player.MaxHealth);
        bar.MaxValue = player.MaxHealth;
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        bar.Value = player.Health;
        if (player.Health <= player.MaxHealth * 0.5 && player.Health > player.MaxHealth * 0.25) { bar.TextureProgress_ = (Texture)bar_yellow; }
        if (player.Health <= player.MaxHealth * 0.25) { bar.TextureProgress_ = (Texture)bar_red; }
        if (player.Health <= 0) { player.isDead(); }

    }
}
