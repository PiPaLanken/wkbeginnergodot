using Godot;
using System;

public class CircleEffect : Area2D
{
    [Export]
    public Vector2 Speed;
    private Vector2 Direction = new Vector2();
    private Vector2 GameSize = new Vector2(100, 74);
    private Random rdm = new Random();
    Node2D Game;

    public Vector2 CellSize = new Vector2(64, 64);



    Timer timer = new Timer();
    TileMap Map;

    public override void _Ready()
    {
        CircleAttack();
        Map = GetNode("/root/Game/TileMap") as TileMap;

    }
    public void CircleAttack()
    {
        int grad = rdm.Next(-90, 90);
        Speed = new Vector2(Mathf.Cos(grad) * 100, Mathf.Sin(grad) * 100);
    }

    public override void _Process(float delta)
    {
        Position = new Vector2(Position.x + Speed.x * delta, Position.y + Speed.y * delta);



    }
    public void bodyEntered(KinematicBody2D Player)
    {
        if (Player.Name == "Player")
        {
            this.QueueFree();
            Player.Call("Damaged", 100);
        }
        else if (Player.Name == "Enemy")
            GD.Print("Not Player");
    }
    public void TileMapEntered(TileMap tileMap)
    {
        if (tileMap.Name == "TileMap")
        {
            GD.Print("Entered TileMap");
        }
    }

}
